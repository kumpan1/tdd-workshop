package diy_stack

type DIYStack interface {
	Pop(inputs []int, op int) []int
}

type diyStack struct {
}

func (s *diyStack) Pop(inputs []int, op int) []int {
	panic("implement me")
}
