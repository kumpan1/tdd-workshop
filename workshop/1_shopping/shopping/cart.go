package shopping

type Item struct {
	Name  string
	Price int
}

type PromoCode struct {
	Code     string
	Discount int
}

type Cart interface {
	AddItem(item Item)
	ListItems() []Item
	AddPromoCode(promoCode PromoCode)
	Checkout() int
}

type cart struct {
	items     []Item
	promoCode PromoCode
}

func (c *cart) AddItem(item Item) {
	panic("implement me")
}

func (c *cart) ListItems() []Item {
	panic("implement me")
}

func (c *cart) AddPromoCode(promoCode PromoCode) {
	panic("implement me")
}

func (c *cart) Checkout() int {
	panic("implement me")
}
