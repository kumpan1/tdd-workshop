package user

import (
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/suite"
	"testing"
)

type HandlerTest struct {
	suite.Suite
	controller *gomock.Controller
	handler    *handler
	service    *MockService
}

func (t *HandlerTest) SetupTest() {
	t.controller = gomock.NewController(t.Suite.T())
	t.service = NewMockService(t.controller)
	t.handler = &handler{
		t.service,
	}
}

func (t *HandlerTest) TearDownTest() {
	t.controller.Finish()
	t.handler = nil
}

func TestHandlerTestSuite(t *testing.T) {
	suite.Run(t, new(HandlerTest))
}

//How to create request path variable
//mapCtx := map[string]string{"id": id}
//request = mux.SetURLVars(request, mapCtx)

func (t *HandlerTest) TestHandler_Method_WhenSomething_ThenSomething() {
	//arrange

	//act

	//assert
}
