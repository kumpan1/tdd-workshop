package employee

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"net/http"
	"strconv"
)

type Handler interface {
	Get(w http.ResponseWriter, r *http.Request)
}

type handler struct {
	service Service
}

func (h *handler) Get(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		w.WriteHeader(400)
		w.Write([]byte("bad request"))
		return
	}

	employee, err := h.service.GetEmployee(id)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	marshal, _ := json.Marshal(employee)

	w.WriteHeader(http.StatusOK)
	w.Write(marshal)
}

func NewHandler() Handler {
	db := NewDB()
	r := &repository{
		db: db,
	}
	s := &service{
		repository: r,
	}
	return &handler{service: s}
}
