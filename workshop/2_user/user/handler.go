package user

import (
	"net/http"
)

type Handler interface {
	Get(w http.ResponseWriter, r *http.Request)
}

type handler struct {
	service Service
}

//how to get id from path variable
//vars := mux.Vars(r)
//id, err := strconv.Atoi(vars["id"])

func (h *handler) Get(w http.ResponseWriter, r *http.Request) {
	panic("imeplement me")
}

func NewHandler() Handler {
	db := NewDB()
	r := &repository{
		db: db,
	}
	s := &service{
		repository: r,
	}
	return &handler{service: s}
}
