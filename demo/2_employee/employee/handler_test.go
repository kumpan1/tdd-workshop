package employee

import (
	"encoding/json"
	"errors"
	"github.com/golang/mock/gomock"
	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
)

type HandlerTest struct {
	suite.Suite
	controller *gomock.Controller
	handler    *handler
	service    *MockService
}

func (t *HandlerTest) SetupTest() {
	t.controller = gomock.NewController(t.Suite.T())
	t.service = NewMockService(t.controller)
	t.handler = &handler{
		t.service,
	}
}

func (t *HandlerTest) TearDownTest() {
	t.controller.Finish()
	t.handler = nil
}

func TestHandlerTestSuite(t *testing.T) {
	suite.Run(t, new(HandlerTest))
}

func (t *HandlerTest) TestHandler_Get_WhenServiceReturnEmployee_ThenReturnStatusOk() {
	//arrange
	id := "1234"
	mapCtx := map[string]string{"id": id}
	w := httptest.NewRecorder()
	r, _ := http.NewRequest(http.MethodGet, "", nil)

	employee := Employee{}
	expectBody, _ := json.Marshal(employee)

	gomock.InOrder(
		t.service.EXPECT().GetEmployee(1234).Return(employee, nil),
	)

	//act
	t.handler.Get(w, mux.SetURLVars(r, mapCtx))

	//assert
	actualBody, _ := ioutil.ReadAll(w.Body)

	assert.Equal(t.T(), w.Code, http.StatusOK)
	assert.Equal(t.T(), expectBody, actualBody)
}

func (t *HandlerTest) TestHandler_Get_WhenIDIsNotNumber_ThenReturnStatus400() {
	//arrange
	id := "id"
	mapCtx := map[string]string{"id": id}
	w := httptest.NewRecorder()
	r, _ := http.NewRequest(http.MethodGet, "", nil)

	gomock.InOrder(
		t.service.EXPECT().GetEmployee(gomock.Any()).Times(0),
	)

	//act
	t.handler.Get(w, mux.SetURLVars(r, mapCtx))

	//assert
	actualBody, _ := ioutil.ReadAll(w.Body)

	assert.Equal(t.T(), w.Code, http.StatusBadRequest)
	assert.Equal(t.T(), []byte("bad request"), actualBody)
}

func (t *HandlerTest) TestHandler_Get_WhenIDIsEmpty_ThenReturnStatus400() {
	//arrange
	id := " "
	mapCtx := map[string]string{"id": id}
	w := httptest.NewRecorder()
	r, _ := http.NewRequest(http.MethodGet, "", nil)

	gomock.InOrder(
		t.service.EXPECT().GetEmployee(gomock.Any()).Times(0),
	)

	//act
	t.handler.Get(w, mux.SetURLVars(r, mapCtx))

	//assert
	actualBody, _ := ioutil.ReadAll(w.Body)

	assert.Equal(t.T(), w.Code, http.StatusBadRequest)
	assert.Equal(t.T(), []byte("bad request"), actualBody)
}

func (t *HandlerTest) TestHandler_Get_WhenServiceError_ThenReturnStatus500() {
	//arrange
	id := "1234"
	mapCtx := map[string]string{"id": id}
	w := httptest.NewRecorder()
	r, _ := http.NewRequest(http.MethodGet, "", nil)

	employee := Employee{}
	error := errors.New("test")

	gomock.InOrder(
		t.service.EXPECT().GetEmployee(1234).Return(employee, error),
	)

	//act
	t.handler.Get(w, mux.SetURLVars(r, mapCtx))

	//assert
	actualBody, _ := ioutil.ReadAll(w.Body)

	assert.Equal(t.T(), w.Code, http.StatusInternalServerError)
	assert.Equal(t.T(), []byte(error.Error()), actualBody)
}

func (t *HandlerTest) TestHandler_NewHandler_WhenInitial_ThenReturnHandlerType() {
	//arrange
	expectType := &handler{}

	//act
	result := NewHandler()

	//assert
	assert.IsType(t.T(), expectType, result)
}
