package diy_stack

import (
	"github.com/stretchr/testify/suite"
	"testing"
)

type DIYStackTest struct {
	suite.Suite
	diyStack DIYStack
}

func (t *DIYStackTest) SetupTest() {
	t.diyStack = &diyStack{}
}

func (t *DIYStackTest) TearDownTest() {
	t.diyStack = nil
}

func TestDIYStackTestSuite(t *testing.T) {
	suite.Run(t, new(DIYStackTest))
}

func (t *DIYStackTest) TestDIYStack_Pop_WhenSomething_ThenReturnSomething() {
	//arrange

	//act

	//assert
}
