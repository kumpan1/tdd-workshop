package employee

//go:generate mockgen -source=./repository.go -destination=./mock_repository.go -package=employee
type Repository interface {
	FindByID(id int) (Employee, error)
}

type repository struct {
	db KumpanatDB
}

func (r *repository) FindByID(id int) (Employee, error) {
	return r.db.Get(id)
}
