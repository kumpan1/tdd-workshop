// Code generated by MockGen. DO NOT EDIT.
// Source: ./kumpanat_db.go

// Package user is a generated GoMock package.
package user

import (
	reflect "reflect"

	gomock "github.com/golang/mock/gomock"
)

// MockKumpanatDB is a mock of KumpanatDB interface.
type MockKumpanatDB struct {
	ctrl     *gomock.Controller
	recorder *MockKumpanatDBMockRecorder
}

// MockKumpanatDBMockRecorder is the mock recorder for MockKumpanatDB.
type MockKumpanatDBMockRecorder struct {
	mock *MockKumpanatDB
}

// NewMockKumpanatDB creates a new mock instance.
func NewMockKumpanatDB(ctrl *gomock.Controller) *MockKumpanatDB {
	mock := &MockKumpanatDB{ctrl: ctrl}
	mock.recorder = &MockKumpanatDBMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use.
func (m *MockKumpanatDB) EXPECT() *MockKumpanatDBMockRecorder {
	return m.recorder
}

// Get mocks base method.
func (m *MockKumpanatDB) Get(id int) (User, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Get", id)
	ret0, _ := ret[0].(User)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// Get indicates an expected call of Get.
func (mr *MockKumpanatDBMockRecorder) Get(id interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Get", reflect.TypeOf((*MockKumpanatDB)(nil).Get), id)
}
