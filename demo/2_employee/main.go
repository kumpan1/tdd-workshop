package main

import (
	"bitbucket.org/kumpan1/tdd-workshop/demo/2_employee/employee"
	"github.com/gorilla/mux"
	"net/http"
	"time"
)

func main() {
	handler := employee.NewHandler()

	r := mux.NewRouter()
	r.HandleFunc("/employees/{id}", handler.Get)

	srv := &http.Server{
		Handler: r,
		Addr:    "127.0.0.1:8080",
		// Good practice: enforce timeouts for servers you create!
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}

	srv.ListenAndServe()
}
