# Employee API #

The REST API for employee Method Get '{url}/employees/{id}'

### Requirement ###

* Having REST api for get employee by id from database.
* return http status 200 if can get employee.
* return http status 400 if id is not number or empty.
