package employee

import (
	"errors"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
	"testing"
)

type ServiceTest struct {
	suite.Suite
	controller *gomock.Controller
	service    *service
	repository *MockRepository
}

func (t *ServiceTest) SetupTest() {
	t.controller = gomock.NewController(t.Suite.T())
	t.repository = NewMockRepository(t.controller)
	t.service = &service{
		t.repository,
	}
}

func (t *ServiceTest) TearDownTest() {
	t.controller.Finish()
	t.service = nil
}

func TestServiceTestSuite(t *testing.T) {
	suite.Run(t, new(ServiceTest))
}

func (t *ServiceTest) TestService_GetEmployee_WhenGetRepository_ThenReturnEmployee() {
	//arrange
	id := 1234
	employee := Employee{
		ID:       1234,
		Username: "1234",
		Age:      12,
	}

	t.repository.EXPECT().FindByID(id).Return(employee, nil)

	//act
	result, err := t.service.GetEmployee(id)

	//assert
	assert.Nil(t.T(), err)
	assert.Equal(t.T(), employee, result)
}

func (t *ServiceTest) TestService_GetEmployee_WhenGetRepositoryError_ThenReturnError() {
	//arrange
	id := 1234
	employee := Employee{}

	t.repository.EXPECT().FindByID(id).Return(employee, errors.New("test"))

	//act
	_, err := t.service.GetEmployee(id)

	//assert
	assert.NotNil(t.T(), err)
}
