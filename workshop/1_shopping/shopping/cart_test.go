package shopping

import (
	"github.com/stretchr/testify/suite"
	"testing"
)

type CartTest struct {
	suite.Suite
	cart Cart
}

func (t *CartTest) SetupTest() {
	t.cart = &cart{}
}

func (t *CartTest) TearDownTest() {
	t.cart = nil
}

func TestCartTestSuite(t *testing.T) {
	suite.Run(t, new(CartTest))
}

func (t *CartTest) TestCart_Method_WhenSomething_ThenSomething() {
	//arrange

	//act

	//assert
}
