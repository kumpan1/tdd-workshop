package employee

import (
	"errors"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
	"testing"
)

type RepositoryTest struct {
	suite.Suite
	controller *gomock.Controller
	repository *repository
	db         *MockKumpanatDB
}

func (t *RepositoryTest) SetupTest() {
	t.controller = gomock.NewController(t.Suite.T())
	t.db = NewMockKumpanatDB(t.controller)
	t.repository = &repository{
		t.db,
	}
}

func (t *RepositoryTest) TearDownTest() {
	t.controller.Finish()
	t.repository = nil
}

func TestRepositoryTestSuite(t *testing.T) {
	suite.Run(t, new(RepositoryTest))
}

func (t *RepositoryTest) TestRepository_FindByID_WhenDBGet_ThenReturnEmployee() {
	//arrange
	id := 1234
	employee := Employee{
		ID:       1234,
		Username: "1234",
		Age:      12,
	}

	t.db.EXPECT().Get(id).Return(employee, nil)

	//act
	result, err := t.repository.FindByID(id)

	//assert
	assert.Nil(t.T(), err)
	assert.Equal(t.T(), employee, result)
}

func (t *RepositoryTest) TestService_GetEmployee_WhenDBGetError_ThenReturnError() {
	//arrange
	id := 1234
	employee := Employee{}

	t.db.EXPECT().Get(id).Return(employee, errors.New("test"))

	//act
	_, err := t.repository.FindByID(id)

	//assert
	assert.NotNil(t.T(), err)
}
