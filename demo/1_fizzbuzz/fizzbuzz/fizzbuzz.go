package fizzbuzz

import "fmt"

type FizzBuzz interface {
	FizzBuzz(int) string
}

type fizzbuzz struct {
}

func (f *fizzbuzz) FizzBuzz(i int) string {
	if f.isFizzBuzz(i) {
		return "FizzBuzz"
	}
	if f.isFizz(i) {
		return "Fizz"
	}
	if f.isBuzz(i) {
		return "Buzz"
	}

	return fmt.Sprintf("%d", i)
}

func (f *fizzbuzz) isBuzz(i int) bool {
	return i%5 == 0
}

func (f *fizzbuzz) isFizz(i int) bool {
	return i%3 == 0
}

func (f *fizzbuzz) isFizzBuzz(i int) bool {
	return i%5 == 0 && i%3 == 0
}
