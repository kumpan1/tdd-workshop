package user

//go:generate mockgen -source=./kumpanat_db.go -destination=./mock_kumpanat_db.go -package=user
type KumpanatDB interface {
	Get(id int) (User, error)
}

type kumpanatDB struct {
}

func (k *kumpanatDB) Get(id int) (User, error) {
	var user User

	user = User{
		ID:       id,
		Username: "kum",
	}

	return user, nil
}

func NewDB() KumpanatDB {
	return &kumpanatDB{}
}
