# FizzBuzz #

The FizzBuzz problem is a classic test given in coding interviews.

### Requirement ###

* Return string “Fizz” if input is divisible by 3.
* Return string “Buzz” if input is divisible by 5.
* Return string “FizzBuzz” if input is divisible by 3 and 5.
* Return the input string if input cannot be divisible by 3 and 5.
