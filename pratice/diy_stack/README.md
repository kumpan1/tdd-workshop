# DIY Stack #

The simple stack must have pop(), push() method but this practice need to implement your own stack that have only one
function Pop() with operation.

### Interface function ###

Pop(inputs []int, operation int) []int

### Requirement ###

* operation == 1 -> remove the first element
* operation == 2 -> remove the last element
* operation == 3 -> remove the middle of element. If input length is even number, removes less index.

### Example ###

* operation 1 -> input []int{2,4,1,3} should return []int{4,1,3}
* operation 2 -> input []int{2,4,1,3} should return []int{2,4,1}
* operation 3 even input -> input []int{2,4,1,3} should return []int{2,1,3}
* operation 3 odd input -> input []int{2,4,1,3,5} should return []int{2,4,3,5}
* operation 9999 -> input []int{2,4,1,3,5} should return []int{2,4,1,3,5}