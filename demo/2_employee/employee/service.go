package employee

type Service interface {
	GetEmployee(id int) (Employee, error)
}

//go:generate mockgen -source=./service.go -destination=./mock_service.go -package=employee
type service struct {
	repository Repository
}

func (s *service) GetEmployee(id int) (Employee, error) {
	return s.repository.FindByID(id)
}
