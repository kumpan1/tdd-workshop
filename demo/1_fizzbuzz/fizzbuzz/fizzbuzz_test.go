package fizzbuzz

import (
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
	"testing"
)

type FizzBuzzTest struct {
	suite.Suite
	fizzBuzz FizzBuzz
}

func (t *FizzBuzzTest) SetupTest() {
	t.fizzBuzz = &fizzbuzz{}
}

func (t *FizzBuzzTest) TearDownTest() {
	t.fizzBuzz = nil
}

func TestFizzBuzzTestSuite(t *testing.T) {
	suite.Run(t, new(FizzBuzzTest))
}

func (t *FizzBuzzTest) TestFizzBuzz_FizzBuzz_WhenInput6_ThenReturnFizz() {
	//arrange
	input := 6

	//act
	result := t.fizzBuzz.FizzBuzz(input)

	//assert
	assert.Equal(t.T(), "Fizz", result)
}

func (t *FizzBuzzTest) TestFizzBuzz_FizzBuzz_WhenInput10_ThenReturnBuzz() {
	//arrange
	input := 10

	//act
	result := t.fizzBuzz.FizzBuzz(input)

	//assert
	assert.Equal(t.T(), "Buzz", result)
}

func (t *FizzBuzzTest) TestFizzBuzz_FizzBuzz_WhenInput15_ThenReturnFizzBuzz() {
	//arrange
	input := 15

	//act
	result := t.fizzBuzz.FizzBuzz(input)

	//assert
	assert.Equal(t.T(), "FizzBuzz", result)
}

func (t *FizzBuzzTest) TestFizzBuzz_FizzBuzz_WhenInput7_ThenReturn7() {
	//arrange
	input := 7

	//act
	result := t.fizzBuzz.FizzBuzz(input)

	//assert
	assert.Equal(t.T(), "7", result)
}
