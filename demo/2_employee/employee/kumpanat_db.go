package employee

import "errors"

//go:generate mockgen -source=./kumpanat_db.go -destination=./mock_kumpanat_db.go -package=employee
type KumpanatDB interface {
	Get(id int) (Employee, error)
}

type kumpanatDB struct {
}

func (k *kumpanatDB) Get(id int) (Employee, error) {
	var employee Employee

	if id != 1234 {
		return employee, errors.New("not found")
	}

	employee = Employee{
		ID:       id,
		Username: "Kumpanat",
		Age:      28,
	}

	return employee, nil
}

func NewDB() KumpanatDB {
	return &kumpanatDB{}
}
