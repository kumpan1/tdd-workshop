# User API #

The REST API for user Method Get '{url}/users/{id}'.

### Requirement ###

* Having REST api for get user by id from database.
* return http status 200 if can get user.
* return http status 400 if id is not number or empty.
