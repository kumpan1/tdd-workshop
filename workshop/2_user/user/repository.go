package user

//go:generate mockgen -source=./repository.go -destination=./mock_repository.go -package=user
type Repository interface {
	FindByID(id int) (User, error)
}

type repository struct {
	db KumpanatDB
}

func (r *repository) FindByID(id int) (User, error) {
	panic("implement me")
}
