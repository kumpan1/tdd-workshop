package user

type Service interface {
	GetUser(id int) (User, error)
}

//go:generate mockgen -source=./service.go -destination=./mock_service.go -package=user
type service struct {
	repository Repository
}

func (s *service) GetUser(id int) (User, error) {
	return s.repository.FindByID(id)
}
