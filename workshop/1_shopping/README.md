# Shopping cart #

The shopping cart can add an item to cart and have checkout method that sums the price of items. It can calculate
discounts by using a promo code.

### Requirement ###

* Having checkout method that can sum price of items.
* Checkout method can add promo code for discount 0-100%.
