package user

import (
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/suite"
	"testing"
)

type ServiceTest struct {
	suite.Suite
	controller *gomock.Controller
	service    *service
	repository *MockRepository
}

func (t *ServiceTest) SetupTest() {
	t.controller = gomock.NewController(t.Suite.T())
	t.repository = NewMockRepository(t.controller)
	t.service = &service{
		t.repository,
	}
}

func (t *ServiceTest) TearDownTest() {
	t.controller.Finish()
	t.service = nil
}

func TestServiceTestSuite(t *testing.T) {
	suite.Run(t, new(ServiceTest))
}

func (t *ServiceTest) TestService_GetUser_WhenSomething_ThenReturnSomething() {
	//arrange

	//act

	//assert
}
