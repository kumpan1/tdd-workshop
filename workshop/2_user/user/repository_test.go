package user

import (
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/suite"
	"testing"
)

type RepositoryTest struct {
	suite.Suite
	controller *gomock.Controller
	repository *repository
	db         *MockKumpanatDB
}

func (t *RepositoryTest) SetupTest() {
	t.controller = gomock.NewController(t.Suite.T())
	t.db = NewMockKumpanatDB(t.controller)
	t.repository = &repository{
		t.db,
	}
}

func (t *RepositoryTest) TearDownTest() {
	t.controller.Finish()
	t.repository = nil
}

func TestRepositoryTestSuite(t *testing.T) {
	suite.Run(t, new(RepositoryTest))
}

func (t *RepositoryTest) TestRepository_FindByID_WhenSomething_ThenSomething() {
	//arrange

	//act

	//assert
}
